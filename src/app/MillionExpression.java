package app;

public class MillionExpression extends Expression{

    @Override
    public String One() {
        return "F";
    }

    @Override
    public String Four() {
        return "FH";
    }

    @Override
    public String Five() {
        return "H";
    }

    @Override
    public String Nine() {
        return "FZ";
    }

    @Override
    public int Multiplier() {
        return 1000000;
    }

}
