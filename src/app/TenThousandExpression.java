package app;

public class TenThousandExpression extends Expression{

    @Override
    public String One() {
        return "N";
    }

    @Override
    public String Four() {
        return "NW";
    }

    @Override
    public String Five() {
        return "W";
    }

    @Override
    public String Nine() {
        return "NP";
    }

    @Override
    public int Multiplier() {
        return 10000;
    }

}
