package app;

public class HundredThousandExpression extends Expression{

    @Override
    public String One() {
        return "P";
    }

    @Override
    public String Four() {
        return "PB";
    }

    @Override
    public String Five() {
        return "B";
    }

    @Override
    public String Nine() {
        return "PF";
    }

    @Override
    public int Multiplier() {
        return 100000;
    }

}
