package app;

public class ThousandExpression extends Expression{

    @Override
    public String One() {
        return "M";
    }

    @Override
    public String Four() {
        return "ME";
    }

    @Override
    public String Five() {
        return "E";
    }

    @Override
    public String Nine() {
        return "MN";
    }

    @Override
    public int Multiplier() {
        return 1000;
    }

}